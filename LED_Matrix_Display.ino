/*
0	cat 3
1	cat 5
2	cat 4
3	an 1
4	cat 1
5	cat 2
6	an 3
7	an 5
8	an 2
9	an 4
10	an 1 (2)
11	an 2 (2)
12	an 6
13	an 7
14	an 3 (2)
15	an 5 (2)
16	an 4 (2)
17	cat 3 (2)
18	cat 5 (2)
19	cat 4 (2)
20	cat 1 (2)
21	cat 2 (2)
22	an 6 (2)
23	an 7 (2)
*/

#define REFRESH 1
#define ADVANCE_DELAY 100

void sendSerial();
void zeroSerial();
void setRow(int j,int k);
void scanDisplay();

const int data = 13;
const int clock = 12;
const int latch = 11;
const int serialArrayZeroed[] = {1,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0};
const int columnArray[] = {3,8,6,9,7,12,13,10,11,14,16,15,22,23};
int serialArray[] = {0,0,0,1,0,0,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,1,1};
int currentDisplay[5][14];
int totalDisplay[5][500] = {0};
int index;

void setup()
{
  pinMode(data, OUTPUT);
  pinMode(clock, OUTPUT);
  pinMode(latch, OUTPUT);
  digitalWrite(clock, LOW);
  digitalWrite(latch, LOW);
  delay(1000);
  sendSerial();
  delay(2000);
  zeroSerial();
  sendSerial();
  for(int i = 0; i < 14; i++) totalDisplay[2][i] = 1;

  for(int i=0;i<14;i++){
   for(int j=0;j<5;j++){
      currentDisplay[j][i] =0;
    }
  }
  //Serial.begin(9600);
}

void loop()
{
  int index = 0;
 //int zeroCount = 0;
  while(1){
    for (int i = 0; i < ADVANCE_DELAY; i++){
      scanDisplay();
    }
    boolean allZero = true;
    for(int i=0;i<14;i++){
      for(int j=0;j<5;j++){
        int newIndex = i+index;
        currentDisplay[j][i] = totalDisplay[j][newIndex];
        if (totalDisplay[j][newIndex] == 1){
          //currentDisplay[j][i] = 1;
          allZero = false;
        }
      }
    }
    if(allZero != false) break;
    index++;
    //Serial.println(index);
  }

}

void scanDisplay()
{
  for(int i = 0; i < 7; i++)
  {
     zeroSerial();
     serialArray[columnArray[i]] = 1; 
     serialArray[columnArray[i+7]] = 1;
     setRow(i,i+7);
     sendSerial();
     delay(REFRESH);
  }
}

void setRow(int j, int k)
{
  if(currentDisplay[0][j] == 1) serialArray[4] = 0;
  if(currentDisplay[0][k] == 1) serialArray[20] = 0;
  if(currentDisplay[1][j] == 1) serialArray[5] = 0;
  if(currentDisplay[1][k] == 1) serialArray[21] = 0;
  if(currentDisplay[2][j] == 1) serialArray[0] = 0;
  if(currentDisplay[2][k] == 1) serialArray[17] = 0;
  if(currentDisplay[3][j] == 1) serialArray[2] = 0;
  if(currentDisplay[3][k] == 1) serialArray[19] = 0;
  if(currentDisplay[4][j] == 1) serialArray[1] = 0;
  if(currentDisplay[4][k] == 1) serialArray[18] = 0;
}

void zeroSerial()
{
    for(int i = 0; i < 24; i++)
  {
    serialArray[i] = serialArrayZeroed[i];
  }
}

void sendSerial()
{
  digitalWrite(latch, LOW);
  delayMicroseconds(15);
  for(int i = 23; i >= 0; i--) 
  {
    if(serialArray[i] == 1)
    {
      digitalWrite(data, HIGH);
    } else {
      digitalWrite(data, LOW);
    }
    delayMicroseconds(15);
    digitalWrite(clock, HIGH);
    delayMicroseconds(15);
    digitalWrite(clock, LOW);
  }
  delayMicroseconds(15);
  digitalWrite(latch, HIGH);
}

